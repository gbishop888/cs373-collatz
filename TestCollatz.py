#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(50, 500)
        self.assertEqual(v, 144)

    def test_eval_2(self):
        v = collatz_eval(99, 98)
        self.assertEqual(v, 26)

    def test_eval_3(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_4(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_5(self):
        v = collatz_eval(400, 22)
        self.assertEqual(v, 144)

    def test_eval_6(self):
        v = collatz_eval(777, 777)
        self.assertEqual(v, 34)

    def test_eval_7(self):
        v = collatz_eval(56, 89767)
        self.assertEqual(v, 351)

    def test_eval_8(self):
        v = collatz_eval(1000, 2000)
        self.assertEqual(v, 182)

    def test_eval_9(self):
        v = collatz_eval(123456, 654321)
        self.assertEqual(v, 509)

    def test_eval_10(self):
        v = collatz_eval(999, 2001)
        self.assertEqual(v, 182)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "1 10 11\n100 200 300\n201 210 411\n900 1000 1900\n")

# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
